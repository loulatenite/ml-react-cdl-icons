import React, { Component } from 'react';
import MLIcon from '../../dist/MLIcons.js';
import styled from 'styled-components';

const Container = styled.div`
  text-align: center;
`;
const AppHeader = styled.div`
  background-color: #DA1B2C;
  border-bottom: 8px solid #231f20;
  height: 60px;
  padding: 20px;
  color: white;
`;
const IconBoxWrap = styled.ul`
  display: flex;
  flex-flow: row wrap;
  margin-top: 20px;
`;
const IconBox = styled.li`
  list-style: none;
  width: 128px;
  height: 96px;
`;
const Label = styled.div`
  font-size: 10pt;
  margin-top: 10px;
`;

class App extends Component {
  render() {
    return (
      <Container>
        <AppHeader>
          <h2>ml-react-cdl-icons demo</h2>
        </AppHeader>
        
        <IconBoxWrap>
          {
            MLIcon.listIcons.map((iconType, i) => {
              return (
                <IconBox key={i}>
                  <MLIcon fill="#231f20" type={iconType} />
                  <Label>{ iconType }</Label>
                </IconBox>
                )
            })
          }
        </IconBoxWrap>
      </Container>
    );
  }
}

export default App;
