# Macmillan Learning React CDL Icon Component v1.3.0

# Installation
```
npm install ml-react-cdl-icons --registry  http://npm.macmillantech.com/
```
Must be within the VPN to install.

# Changelog

### ml-react-cdl-icons v1.3.0 :package: Major Release 

* Add jest/enzyme test array to iterate through components and test
* Add demo folder, cd demo && yarn install && yarn start
* Refactor as pure component
* Update SVG attributes for a11y best practices
* Add scale property, pass a number to scale the SVG
* Add propTypes

Icons updated

* Add circle_check_outline
* Add folder
* Add grabber
* Add pause_solo
* Add play_solo
* Add replay
* Add stop_solo
* Add unlock
* Update trash

# Sample usage:
```
import MLIcon from 'ml-react-cdl-icons';
<MLIcon 
	title="add note"
	fill="#ffffff" 
	type="edit" 
	width="24" 
	height="24"
	scale={1.2}
	viewBox="0 0 24 24"
	className="icon" />
```

## PropTypes
```
type: string.isRequired, // One of the list below
title: string, // Title for accessibility
fill: string, // CSS compatible color string
scale: number, // Force scale of svg
width: string, // string without px
height: string
```

## Methods

`MLIcon.listIcons` will return the full array of icon types for testing.

## Icon Types
```
alert_outline
alert
arrow_left
arrow_right
bar_chart
book
bookmark_outline
bookmark
box
calendar
cancel
caret_down
check
checkbox
chevron_down
chevron_left
chevron_right
chevron_up
circle_check_outline
clipboard
clock
comment_highlight
comment_microphone
comment_question
comment_text
comment_thumbs_up
comment
cursor
dock_left
dock_right
dock_top
document
download
edit
eraser
expand
folder
grabber
grid_4
grid_9
head
heart_outline
heart
help_outline
help
highlight
home
info
info_outline
item_add
item_copy
item_edit
item_remove
label_on
label_off
link
list_left
list_right
maximize
menu
microphone
minimize
minus
more
mortar_board
move
not
pause
pause_solo
person
play
play_solo
plus
pointer_outline
pointer
print
replay
rotate_x
rotate_y
search
spreadsheet
stop
stop_solo
text_size
thumbs_down
thumbs_up
tools
trash
trending
upload
unlock
video
x
zoom_in
zoom_out
```

# Testing

`npm run test` or `npm run test:watch` to run jest/enzyme tests. Additional tests welcome.


# Run local demo

The demo folder contains a basic demo server based on `create-react-app`. Presently the demo page imports the compiled version MLIcons.js from the `dist/` folder instead of `src/`. I think this is a limitation of create-react-app's implementation of webpack.
Run `npm run prepublish` to build the module.

```
cd demo && yarn install && yarn start
```