import React from 'react';
import { mount, shallow } from 'enzyme';
import App from '../demo/src/App';

it(`should match the snapshot`, () => {
  const wrapper = mount(
    <App />
  )
  expect(wrapper).toMatchSnapshot();
});
