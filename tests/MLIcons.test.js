import React from 'react';
import { mount, shallow } from 'enzyme';

import MLIcon from '../src/MLIcons';

for (const iconType of MLIcon.listIcons) {
  describe(`${iconType} should render`, () => {
    it(`should have the ${iconType} type prop`, () => {
      const wrapper = mount(
        <MLIcon type={ iconType } />
      )
      expect(wrapper.prop('type')).toEqual( iconType );
    });
    it(`should match the snapshot`, () => {
      const wrapper = mount(
        <MLIcon type={ iconType } />
      )
      expect(wrapper).toMatchSnapshot();
    });
    
  });
  
}

